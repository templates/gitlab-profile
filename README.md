This group contains templates for use within ASTRON.

This repository contains common conventions and tools shared between the
software teams of ASTRON.

GitLab conventions:

- [CI/CD Baseline](https://git.astron.nl/groups/templates/-/wikis/Pipeline%20Baseline)
  - [Continuous Delivery (CD) Guidelines](https://git.astron.nl/groups/templates/-/wikis/Continuous%20Delivery%20Guideline)
  - [Using CI/CD to create commits and tags](https://git.astron.nl/groups/templates/-/wikis/Using-CI-CD-to-create-commits-and-tags)
- Security and user management
  - [Gitlab Repository Configuration](https://git.astron.nl/groups/templates/-/wikis/Gitlab%20Repository%20Configuration)
  - [Gitlab Runner Configuration](https://git.astron.nl/groups/templates/-/wikis/Gitlab%20Runner%20Configuration)
- Gitlab Configuration
  - [Cleanup Docker Registry Images](https://git.astron.nl/groups/templates/-/wikis/Cleanup-Docker-Registry-Images)
  - [Setting up Protected Version Tags](https://git.astron.nl/groups/templates/-/wikis/Setting-up-Protected-Version-Tags)
- [Versioning](https://git.astron.nl/groups/templates/-/wikis/Versioning)
- Proposal [Labeling Docker Images](https://git.astron.nl/groups/templates/-/wikis/Docker%20Image%20Labels)
- [Licensing](https://git.astron.nl/groups/templates/-/wikis/Licensing)
- [Feedback](https://git.astron.nl/groups/templates/-/wikis/Feedback)

GitLab utilities:

- [Purge old jobs](https://git.astron.nl/templates/gitlab-job-purge)
